#ifndef Classes_Cell_h
#define Classes_Cell_h




#include "Prey.h"

#include <random>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <tuple>
#include <string>

class Prey;
//class Parameter;

typedef std::vector<int> I_vector;
typedef std::vector<float> Fl_vector;
typedef std::vector<Prey*> P_vector;
typedef std::vector<Prey*>::iterator P_Iter;

extern std::default_random_engine generator;
extern int Size;
extern int Nn;
// means and sds are used to generate the value of resources and predation risk within the grid.
extern float sd_resources;
extern float sd_predation;
extern float mean_resources;  
extern float mean_predation;
class Cell
{
	public:
		Cell(int, int);
		Cell(const Cell &);
	    	~Cell();
	    	void operator=(const Cell &);

		float get_resources();
		float get_predation_risk();
		float Correlated_value(float, float);
		static int x_value; // static member to keep track of X coordinate
		static int y_value; // static member to keep track of Y coordinate
		P_vector dispersing_preys; // store migrants from a territory;

		void add_prey(const Prey &);
		void remove_prey(float, float);
		void set_group_size();
		void set_dispersing_preys(std::string stg);
		void set_fitness_if_eating();
		void add_offspring(float, float);

		int get_X_coord();
		int get_Y_coord();
		int get_group_size();
		P_vector get_preys_vec();

protected:
		float Resources;
		float Predation_risk;
		int X_coord; // horizontal coordinate
		int Y_coord; // vertical coordinate
		int Group_size; // Number of preys in a cell
		P_vector Preys_vec; // store all preys in a vector


private:
		void free();
		void copy(const Cell &);
		void init_prey();
};

#endif
