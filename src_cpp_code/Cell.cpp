#include "Cell.h"
#include "Prey.h"

#include <random>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <tuple>
#include <string>

class Prey;

extern int Size;
extern int Nn;
extern float sd_resources;
extern float sd_predation;
extern float mean_resources;
extern float mean_predation;

extern std::default_random_engine generator;

Cell::Cell(int x_value, int y_value) // default constructor
{
	std::normal_distribution<float> distribution_resources(mean_resources, sd_resources);
	std::normal_distribution<float> distribution_predation(mean_predation, sd_predation);
	Resources = distribution_resources(generator);
	Predation_risk = distribution_predation(generator);
	X_coord = x_value;
	Y_coord = y_value;
	Preys_vec.reserve(1000);
    init_prey();
	Group_size = -9;
	//std::cout << " cell X " << X_coord << ", Y " << Y_coord << std::endl;
}
Cell::Cell(const Cell &cq) // copy constructor
{
	copy(cq);
	//std::cout << "Cell cc'tor" << std::endl;
}
Cell::~Cell() //deconstructor
{
	free();
	std::cout << "Cell deconstructor" << std::endl;
}
void Cell::operator=(const Cell &cq)
{
	if (this != &cq)
	{
		free();
		copy(cq);
	}
	std::cout << "Cell: assign" << std::endl;
}
void Cell::free()
{
	for (int i = 0; i < Preys_vec.size(); i++)
	{
		delete Preys_vec[i];
		Preys_vec[i] = NULL;
	}
	Preys_vec.clear();
}
void Cell::copy(const Cell &cq)
{
	X_coord  = cq.X_coord;
	Y_coord = cq.Y_coord;
	Group_size = cq.Group_size;
	Resources = cq.Resources;
	Predation_risk = cq.Predation_risk;
	for(std::vector<Prey *>::const_iterator it = cq.Preys_vec.begin(); it < cq.Preys_vec.end(); ++it) Preys_vec.push_back(new Prey(*(*it)));
}


void Cell::init_prey()
{
//initialize each cell with n preys
	for (int i = 0; i < Nn; i++)
	{
		Prey *pP = new Prey(X_coord, Y_coord); //
		Preys_vec.push_back(pP);
	}
}

void Cell::add_prey(const Prey &pp)
{
// function to copy a prey to a Cell
	Preys_vec.push_back(new Prey(pp));
}

void Cell::remove_prey(float mortality_risk_when_eating, float contrast_when_vigilant)
{
// function removes preys from a cell because they were killed.
	for(P_Iter it=Preys_vec.begin(); it!=Preys_vec.end();)
	{
		(*it)->set_mortality(Predation_risk, mortality_risk_when_eating, contrast_when_vigilant);
		if( (*it)->get_mortality() == 1 )
		{
			delete * it;
			*it = NULL;
			it=Preys_vec.erase(it);
			//std::cout << "Remove dead individual" << std::endl;
		}
		else
    		++it;
	}
}

void Cell::add_offspring(float sd_change_when_mutating, float mutation_rate)
{
	// function to add offspring to a cell and remove the mother
	// Remember to apply after set_fitness_if_eating()
	// and (possibly?) also after remove_prey(float, float);
	// When reproduction happens do NOT implement dispersal!
	// Here, pass as argument the chosen value of correlation between vigilance of mother and offsprings

	I_vector all_offspring_number, all_mum_ids;
	Fl_vector all_mum_vigilance;
	typedef std::vector<float> Fl_vector;

	for(P_Iter it=Preys_vec.begin(); it!=Preys_vec.end();)
	{
		(*it)->set_offspring();
		// store data about offspring in vectors
		int offspring_number = (*it)->get_offspring();
		//std::cout << "add_offspring(): number of offspring: " << offspring_number << std::endl;
		int mum_id = (*it)->get_id();
		float mum_vigilance = (*it)->get_vigilance();
		all_offspring_number.push_back(offspring_number);
		all_mum_ids.push_back(mum_id);
		all_mum_vigilance.push_back(mum_vigilance);

		//remove the mother from the vector of pointers to Prey
		delete * it;
		*it = NULL;
		it=Preys_vec.erase(it);
	}

	// add the new offsprings
	//std::cout << " all_offspring_number.size() " << all_offspring_number.size() << std::endl;
	for(int i = 0; i < all_offspring_number.size(); i++)
	{
		for(int j = 0; j < all_offspring_number[i]; j++)
		{
			Prey * pr = new Prey(X_coord, Y_coord, all_mum_ids[i], all_mum_vigilance[i], sd_change_when_mutating, mutation_rate);
			//std::cout << "offspring id " << pr->get_id() << " has vigilance = " << pr->get_vigilance() << "; mum vigilance = " << all_mum_vigilance[i] << std::endl;
			Preys_vec.push_back(pr);
		}
	}
	//std::cout << "add_offspring(): check size of Preys_vec after birth : " << Preys_vec.size() << std::endl;

}

void Cell::set_group_size()
{
	// count number of alive preys in a cell
	int count = 0;

	for(P_Iter it=Preys_vec.begin(); it!=Preys_vec.end(); it++)
	{
  		int is_alive = 1 - (*it)->get_mortality();
  		count = count + is_alive;
	}
	Group_size = count;
}

void Cell::set_fitness_if_eating()
{
// function to increase fitness, if eating
	int count = 0; // variable to count number of preys that choose to eat in a given cell

	for(P_Iter it=Preys_vec.begin(); it!=Preys_vec.end(); it++)
	{
		(*it)->set_chosen_behaviour();
  		std::string eating = (*it)->get_chosen_behaviour();
		//std::cout << "Cell::set_fitness_if_eating(): chosen behavior " << eating << std::endl;
		if(eating == "eat")
		{
  			count = count + 1;
		}
	}
	//std::cout << "set_fitness_if_eating(): count how many preys eat " << count << std::endl;
	double shared_resources = Resources / count; // eating preys equally share resources, scramble competition; if no one eats, shared_resources == Inf
    //std::cout << "Resources = " << Resources << " and count " << count << std::endl;
	for(P_Iter it=Preys_vec.begin(); it!=Preys_vec.end(); it++)
	// This part of the body is not very efficient when the number of preys is large! room for improvement
	{
  		std::string eating = (*it)->get_chosen_behaviour();
		//std::cout << "2: set_fitness_if_eating(): chosen behavior: " << eating << std::endl;
		if(eating == "eat")
		{
  			(*it)->set_fitness(shared_resources);
			//std::cout << "in Cell::set_fitness_if_eating(), for id " << (*it)->get_id() << " shared resource = " << shared_resources << ", updated fitness: " << (*it)->get_fitness() << std::endl;
		}
	}
}

void Cell::set_dispersing_preys(std::string stg)
{
//function  returns dispersing preys with removal
	P_vector dp;
	for(P_Iter it = Preys_vec.begin(); it != Preys_vec.end();)
	{
		//std::cout << "set_dispersing_preys(): the prey moves: " << std::endl;
		if(stg == "easy_random_walk")
		{
			(*it)->easy_random_walk(X_coord, Y_coord);
		}
		else
		{
			std::cout << "Error in set_dispersing_preys(): random walk could be only easy_random_walk. Other walks such as levy_jumps are not yet implemented" << std::endl;
			exit(0);
		}
			dp.push_back(new Prey( **it));	 //copy the individual to the migrants vector
			delete * it;
			*it = NULL;
			it=Preys_vec.erase(it);
	}

	dispersing_preys = dp;
}



float Cell::get_resources() { return Resources; }
float Cell::get_predation_risk() { return Predation_risk; }
int Cell::get_X_coord() { return X_coord; }
int Cell::get_Y_coord() { return Y_coord; }
int Cell::get_group_size() { return Group_size; }
P_vector Cell::get_preys_vec() { return Preys_vec; }
