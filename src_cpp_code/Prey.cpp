#include "Prey.h"

#include <random>
#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>
#include <sstream>
#include <string>
#include <map>
#include <list>

extern int Size;
extern float slope_reproduction_fitness;
extern std::default_random_engine generator;
std::normal_distribution<float> normal_0_1(0,1);
std::uniform_int_distribution<int> distribution_number_of_steps(0, Size);
std::uniform_int_distribution<int> distribution_dir(0, 3);
std::uniform_int_distribution<int> unif_0_1(0, 1);

/*=============================================================================
// all Parameters;
==============================================================================*/



Prey::Prey(int x, int y)
{
	Id = Number_of_preys + 1;
	Number_of_preys++;
	Mortality = 0;
	Destination_cell_X = x;
	Destination_cell_Y = y;
	Fitness = 0.0;
	Vigilance = normal_0_1(generator); // randomly assign vigilance level for first generation
	Offspring = 0;
	Mum_id = -9; // assign dummy value at initialization
	Mum_vigilance = -9.0; // assign dummy value at initialization
	Chosen_behaviour = "NA"; // assign dummy value at initialization

}

//Constructor where to pass the mother's infos as argument:
Prey::Prey(int mum_x_loc, int mum_y_loc, int mum_id, float mum_vigilance, float sd_change_when_mutating, float mutation_rate)
{
	Fitness = 0.0;
	Vigilance = correlated_value(mum_vigilance, sd_change_when_mutating, mutation_rate);
	Offspring = 0;
	Mortality = 0;
	Id = Number_of_preys + 1;
	Number_of_preys++;
	Destination_cell_X = mum_x_loc;
	Destination_cell_Y = mum_y_loc;
	Mum_id = mum_id;
	Mum_vigilance = mum_vigilance;
	Chosen_behaviour = "NA"; // assign dummy value at initialization
}

Prey::Prey(const Prey &ind)
{
	copy(ind);
	//std::cout << "ind copy cc'tor" << std::endl;
}

void Prey::copy(const Prey &ind)
{

	Fitness = ind.Fitness;
	Vigilance = ind.Vigilance;
	Offspring = ind.Offspring;
	Mortality = ind.Mortality;
	Id = ind.Id;
	Destination_cell_X = ind.Destination_cell_X;
	Destination_cell_Y = ind.Destination_cell_Y;
	Mum_id = ind.Mum_id;
	Mum_vigilance = ind.Mum_vigilance;
	Chosen_behaviour = ind.Chosen_behaviour;
}

void Prey::operator=(const Prey &ind)
{
	if (this != &ind)
	{
		free();
		copy(ind);
	}
}

Prey::~Prey() { free(); }
void Prey::free() {};


void Prey::set_chosen_behaviour()
{
	float probability = expit(Vigilance);
	std::binomial_distribution<int> binomial(1, probability);
	int choice = binomial(generator);

	//std::cout << "with Vigilange " << Vigilance << "; choice = " << choice << std::endl;
	if(choice == 0)
	{
		Chosen_behaviour = "eat";
	} else if (choice == 1)
	{
		Chosen_behaviour = "vigilance";
	}	else
	{
		std::cout <<  "Error in  Prey::set_chosen_behaviour()" << std::endl;
	}
}

void Prey::set_fitness(float resources)
{
	Fitness = Fitness + exp(resources);
}

void Prey::set_mortality(float predation_risk, float mortality_risk_when_eating, float contrast_when_vigilant)
{
	// calculate the probability a prey is eaten as a function of:
	// the predation risk in the cell
	// the slope of the mortality risk when the prey eats
	// the contrast when an individual is vigilant; because there is an advantage when vigilant this parameter should be negative

	// The probability is used to generate a binomial random value which sets the value of Mortality

	if(contrast_when_vigilant >= 0.0  ||  mortality_risk_when_eating <= 0.0  )
	{
			std::cout << "Error in Prey::set_mortality: Mortality risk should be > 0.0 or Vigilant individuals are expected to have lower mortality risk. Please, set the contrast parameter to be <= 0.0" << std::endl;
			exit(0);
	}
	// Once we obtain
	float mortality_risk;
	if(Chosen_behaviour == "eat")
	{
		mortality_risk = expit(mortality_risk_when_eating * predation_risk);
	}
	else if(Chosen_behaviour == "vigilance")
	{
		mortality_risk = expit((mortality_risk_when_eating + contrast_when_vigilant) * predation_risk);
	}
	else
	{
		std::cout << "Error in Prey::set_mortality: Chosen_behaviour should be either eat or vigilant" << std::endl;
		exit(0);
	}
	std::binomial_distribution<int> binomial(1, mortality_risk);
	Mortality = binomial(generator);
	//std::cout << "set_mortality. is individual dead (1)? " << Mortality << std::endl;
}


void Prey::set_offspring()
{
	if(slope_reproduction_fitness <= 0.0 )
	{
		std::cout << "Errorin Prey::set_offspring: The slope of fitness in the calculation of reproduction should be a positive number" << std::endl;
		exit(0);
	}
	float lambda = slope_reproduction_fitness * Fitness;
	//std::cout << "in Prey::set_offspring() Fitness = " << Fitness << std::endl;
	std::poisson_distribution<int> rpois(lambda);
	Offspring = rpois(generator);
}
void Prey::easy_random_walk(int X_origin, int Y_origin)
{
	// Pls Note: Implement random walk before creating the dispersal pool.
	// Currently applied within the Cell::set_dispersing_preys() function
	// Easy random walk step by step:
	// randomly pick a number of steps between 0 and 10
	// for each step randomply choose a direction between North, South, West and East
	// If a prey tries to go out of the grid, it's pulled back in from the opposite side of the grid.
	// Once the final destination is given assign the coordinates of the destination to the Prey,
	// move the prey to the dispersal pool.
	std::vector<char> dir = {'N', 'S', 'E', 'W'}; // N = north, S = south, etc.
	int Number_steps = distribution_number_of_steps(generator);
	//std::cout << "origin " << X_origin << "," << Y_origin << std::endl;
			for(int i = 0; i < Number_steps; i++)
			{
				int pick = distribution_dir(generator);
				char chosen_dir = dir[pick];
				if(chosen_dir == 'N')
				// if prey tries to go out from grid, it ends un on the opposite side, else move of one cell in chosen direction
				{
					//std::cout << "Move N" << std::endl;
					if(Y_origin == 0) { Y_origin = Size - 1; }
					else { Y_origin = Y_origin - 1; }
				}
				else if(chosen_dir == 'S')
				{
					//std::cout << "Move S" << std::endl;
					if (Y_origin == Size - 1) { Y_origin = 0; }
					else { Y_origin = Y_origin + 1; }
				}
				else if(chosen_dir == 'E')
				{
					//std::cout << "Move E" << std::endl;
					if (X_origin == Size - 1) { X_origin == 0; }
					else { X_origin = X_origin + 1; }
				}
				else if(chosen_dir == 'W')
				{
					//std::cout << "Move W" << std::endl;
					if (X_origin == 0) { X_origin = Size - 1; }
					else { X_origin = X_origin - 1; }
				}
				else
				{
					std::cout << "Error in easy_random_walk(): Direction can be either N, S, W, E" << std::endl;
				}
			}
			// final destination!
			Destination_cell_X = X_origin;
			Destination_cell_Y = Y_origin;
			//std::cout << "destination cell  " << Destination_cell_X << "," << Destination_cell_Y << std::endl;

}


float Prey::correlated_value(float x, float sd, float rate)
{
// Function to generate the vigilance level of the offspring.
// Mutation rate happens at a rate chosen by the user;
// If there is a mutation, a value is drawn from a normal distribution,
// with mean 0 and sd chosen by the user and added
// to the inherited value of the mother.
// If there is no mutation, then the vigilance level of the offspring is equal to its mother.

    std::binomial_distribution<int> binomial(1, rate);
	int switching =  binomial(generator);
	float y;
	if(switching == 1)
	{
		std::normal_distribution<float> normal_mut(0.0, sd);
		float small_mutation = normal_mut(generator);
		y = x + small_mutation;
	}
	else
	{
		y = x;
	}
	return y;
}


/*
// alternative version of function to generate vigilance level of offspring correlated with the vigilance level of the mother
float Prey::correlated_value(float x, float ro)
{
// Function to generate a random values correlated to x
// Note that x and Ran_vigilance are both normally distributed and of equal variance
// Formula y = ro*var1 + sqrt(1- ro^2) * var2 where ro is the correlation chosen
	float Ran_vigilance = normal_0_1(generator);
	float y = ro * x + sqrt(1 - pow(ro, 2)) * Ran_vigilance;
	return y;
}
*/


int Prey::get_id() { return Id; } // function returns Prey age
int Prey::get_mum_id() { return Mum_id; }
int Prey::get_mortality() { return Mortality; } // function returns Prey age
int Prey::get_destination_cell_X() { return Destination_cell_X; }
int Prey::get_destination_cell_Y() { return Destination_cell_Y; }
std::string Prey::get_chosen_behaviour() { return Chosen_behaviour; }
float Prey::get_fitness() { return Fitness; }
float Prey::get_vigilance() { return Vigilance; }
float Prey::get_mum_vigilance() { return Mum_vigilance; }
float Prey::expit(float num) { return(exp(num) / (1 + exp(num))); }
int Prey::get_offspring() { return Offspring; }
