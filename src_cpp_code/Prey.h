#ifndef Classes_Prey_h
#define Classes_Prey_h

#include <random>
#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>
#include <sstream>
#include <string>
#include <map>

extern int Size;
extern std::default_random_engine generator;

class Prey
{
public:

	Prey(int, int); //
    Prey(int, int, int, float, float, float);
	Prey(const Prey &); // copy constructor 
	void operator=(const Prey &); 
	~Prey(); //deconstructor 
	static int Number_of_preys;
	void easy_random_walk(int, int);
	void set_chosen_behaviour();
	void set_fitness(float);
	void set_mortality(float, float, float);
	int get_id();
	int get_mum_id();
	int get_mortality();
	int get_destination_cell_X();
	int get_destination_cell_Y();
	float correlated_value(float, float, float);
	float get_fitness();
	float get_vigilance();
	float get_mum_vigilance();
	int get_offspring();
	std::string get_chosen_behaviour();
	float expit(float); // calculate expit
	void set_offspring();

protected:
	float Fitness; // fitness level defined as the amount of resources gathered in its life-time
	float Vigilance; // Vigilance probability
	float Mum_vigilance; // Vigilance probability
	int Offspring; // number of offspring produced
	int Mortality; // mortality due to predation
    int Id; // Prey unique identifier;
	int Destination_cell_X;
	int Destination_cell_Y;
	int Mum_id; // keep track of parentage
	std::string Chosen_behaviour; // either "eat" (if eating) or "vigilance" (if vigilant). Options are mutually exclusive.

private:
	void free();
	void copy(const Prey &);
};

#endif
