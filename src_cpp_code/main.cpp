#include "Cell.h"
#include "Prey.h"

#include <random>
#include <iostream>
#include <array>
#include <vector>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <sstream>
#include <typeinfo>
#include <chrono>

typedef std::vector<int> I_vector;
typedef std::vector<Cell*>::iterator C_Iter;
typedef std::vector<Prey*> P_vector;
typedef std::vector<Prey*>::iterator P_Iter;

/*
random engine generator

std::random_device rd;
std::default_random_engine generator(rd());
*/
// obtain seed from system clock
std::chrono::high_resolution_clock::time_point tp = std::chrono::high_resolution_clock::now();
unsigned seed = static_cast<unsigned>(tp.time_since_epoch().count());
std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());//altrenatively use (seed);

/*=========================================================================================================
  parameters
=============================================================================================================*/
int Size; // Size of the squared grid, number of cells == Size ^ 2
int Nn; // Number of preys per cell at initialization, so initial population size is equal to Size^2 * Nn
float mortality_risk_when_eating;
float contrast_when_vigilant;
float slope_reproduction_fitness; // slope of fitness in the reproduction calculation
float Sd_mum_offspring_vigilance; // when a mutation happens, how much does the offspring differ from its mother
float Mutation_rate; // probability that a mutation happens
std::string random_walk_of_choice;
int random_walk; // for convenience we save a numeric value in the csv file
int Generation_length; // Prey generation length. Species are semelparous, so this is equal to a maximum age
int Sim_time; // simulation time
float mean_resources; // mean of Resources // means and sds are used to generate the value of resources and predation risk within the grid.
float mean_predation; // mean of Predation_risk
float sd_resources; // standard deviation of Resources
float sd_predation; // standard deviation of Predation_risk

// function to read parameter values from parameters.csv file
// be careful with csv script!
std::map<std::string, float> get_parameters()
{
	std::ifstream pars ("parameters.csv", std::ios::in);
	//if (!pars.is_open())
	//std::cout<< "opened"<< std::endl;
	if (!pars) {
		std::cerr << "ERROR: cannot find/read parameters.csv file " << std::endl;
		exit(1);
	}

	std::string line;
	std::map<std::string, float> params;
	while (getline(pars, line, '\n'))
	{
		std::cout << line << std::endl;
		char sep = ',';
		std::string s = line;
		std::vector<std::string> strVector;
		std::string varName;
		float valueVar;

		for(size_t p = 0, q = 0; p != s.npos; p = q)
		{
			  strVector.push_back(s.substr(p+(p!=0),
			  (q=s.find(sep, p+1))-p-(p!=0)));
		}
		varName = strVector.front();
		valueVar = std::stof(strVector.back());

		params.insert ( std::pair<std::string, float>(varName, valueVar) );
	}

	pars.close();
	std::cout << "params were read from csv" << std::endl;
	return params;

}

/*=========================================================================================================
  create some variables to store results
=============================================================================================================*/
int Prey::Number_of_preys = 0; // Pass value to class Prey to create unique ids to track the pedigree
int Population_size;
int Track_generation_number = 0;
std::vector<Cell*> gclList;
void createCells()
//create the initial squared grid of cells and set the X/Y coordinates
{
	for (int i = 0; i < Size; ++i)
	{
		for (int j = 0; j < Size; ++j)
		{
			gclList.push_back(new Cell(i, j));
	    }
	}
	//store grid info to file
	std::string gridfile = "grid_info.txt";
	std::ofstream grid_txt(gridfile);
	grid_txt << "X_coord" << ", " <<
				"Y_coord" << ", " <<
				"Resources" << ", " <<
				"Predation_risk" <<  std::endl;
	for (int i = 0; i < pow(Size, 2); i++)
	{
		grid_txt <<  gclList[i]->get_X_coord() << ", " <<
			gclList[i]->get_Y_coord() << ", " <<
			gclList[i]->get_resources() << ", " <<
			gclList[i]->get_predation_risk() <<  std::endl;
	}
	grid_txt << std::endl;
	grid_txt.close();
}

/*=========================================================================================================
  Start main
=============================================================================================================*/

int main()
{
	// read parameters from csv file
	std::map<std::string, float> params = get_parameters();
	mortality_risk_when_eating = params["mortality_risk_when_eating"];
	contrast_when_vigilant = params["contrast_when_vigilant"];
	slope_reproduction_fitness = params["slope_reproduction_fitness"];
	Sd_mum_offspring_vigilance = params["Sd_mum_offspring_vigilance"];
	Mutation_rate = params["Mutation_rate"];
	mean_resources = params["mean_resources"];
	mean_predation = params["mean_predation"];
	sd_resources = params["sd_resources"];
	sd_predation = params["sd_predation"];
	// for convenience all parameters are read as variables of type float; I then convert them to the correct type
	random_walk = static_cast<int>(params["random_walk"]);
	Generation_length = static_cast<int>(params["Generation_length"]);
	Sim_time = static_cast<int>(params["Sim_time"]);
	Size = static_cast<int>(params["Size"]);
	Nn = static_cast<int>(params["Nn"]);
	if(random_walk == 0)
	{
		random_walk_of_choice = "easy_random_walk";
	}
	else
	{
		random_walk_of_choice = "NA";
	}


	// store results into a txt file
	std::string namefile = "results_simulation.txt";
	std::ofstream res_text(namefile);
	res_text << "Population_size" << ", " <<
			"Simulation_time" << ", " <<
			"Generation_number" << ", " <<
			"New_generation" << ", " <<
			"X_coord" << ", " <<
			"Y_coord" << ", " <<
			"Resources" << ", " <<
			"Predation_risk" << ", " <<
			// prey level
		    "Prey_id" << ", " <<
			 "Mum_id" << ", " <<
			 "Mum_vigilance" << ", " <<
			 "Fitness" << ", " <<
			 "Vigilance" << std::endl;

	P_vector dispersal_pool;
	dispersal_pool.reserve(1000000);
	createCells();

	/*=========================================================================================================
	  Start loop through time
	=============================================================================================================*/

for(int sim = 0; sim < Sim_time; sim++)
{
	std::cout << "sim time = " << sim << std::endl;
	// every Generation_length allow preys to reproduce, remove all the mothers and add the offsprings
	float remainder = sim % Generation_length;
	int record_reproduction;
	Population_size = 0;
	for (int i = 0; i < gclList.size(); i++)
	{
		// calculate population size at the beginning of a time step
		gclList[i]->set_group_size();
		int gs = gclList[i]->get_group_size();
		Population_size = Population_size + gs;
	}
    // std::cout << "at the begging of time step " << sim << " population size equals " << Population_size << std::endl;
	if(Population_size <= 0)
	{
		std::cout << "Warning: population is extinct! Try different parameter values" << std::endl;
		exit(0);
	}
	for (int i = 0; i < gclList.size(); i++)
	{
		for(int jj=0; jj < gclList[i]->get_group_size(); jj++)
		{
			res_text << Population_size << ", " <<
						sim << ", " <<
						Track_generation_number << ", " <<
						record_reproduction << ", " <<
						gclList[i]->get_X_coord() << ", " <<
						gclList[i]->get_Y_coord() << ", " <<
						gclList[i]->get_resources() << ", " <<
						gclList[i]->get_predation_risk() << ", " <<
						// prey level
						gclList[i]->get_preys_vec()[jj]->get_id() << ", " <<
						gclList[i]->get_preys_vec()[jj]->get_mum_id() << ", " <<
						gclList[i]->get_preys_vec()[jj]->get_mum_vigilance() << ", " <<
						gclList[i]->get_preys_vec()[jj]->get_fitness() << ", " <<
						gclList[i]->get_preys_vec()[jj]->get_vigilance() << std::endl;
		}
	}


	for (int i = 0; i < gclList.size(); i++)
	{
		// allow preys to eat if they choose to
		gclList[i]->set_fitness_if_eating();
		// allow predation to happen and remove dead preys
		gclList[i]->remove_prey(mortality_risk_when_eating, contrast_when_vigilant);
		// calculate group size after removal of deads
		gclList[i]->set_group_size();
		// allow preys to walk around a choose a new cell
		gclList[i]->set_dispersing_preys(random_walk_of_choice);
		// move preys to a dispersal pool
		for(int j = 0; j < gclList[i]->get_group_size(); j++)
		{
			dispersal_pool.push_back(new Prey(*(gclList[i]->dispersing_preys[j])));
		}
	}

	// reassign individuals from the dispersal pool to the selected destitation cell:
	while(dispersal_pool.size()  > 0)
	{
		for(int j = 0; j < dispersal_pool.size(); j++)
		{
			Prey *ind = dispersal_pool[j];
			int where_to_move_X = (ind)->get_destination_cell_X();
			int where_to_move_Y = (ind)->get_destination_cell_Y();
			//std::cout << "where_to_move_X = " << where_to_move_X << " where_to_move_Y " << where_to_move_Y << std::endl;
			for (int i = 0; i < gclList.size(); i++)
			{
				if(gclList[i]->get_X_coord() == where_to_move_X & gclList[i]->get_Y_coord() == where_to_move_Y)
				{
					//std::cout << "adding prey to cell" << std::endl;
					gclList[i]->add_prey(*ind);
				}
			}
			//std::cout << "delete prey from dispersal pool" << std::endl;
			delete dispersal_pool[j];
			dispersal_pool.erase(dispersal_pool.begin()+j); // alternatively, one could delete the whole vector outsite the loop
		}
	}
	//std::cout << "is dispersal pool empty after reassignments:" << std::endl; if (dispersal_pool.empty())  {  std::cout << "True" << std::endl; }  else {  std::cout << "False" << std::endl; }

		// every Generation_length allow preys to reproduce, remove all the mothers and add the offsprings
		if(sim != 0 & remainder == 0 )
		{
			std::cout << " reproduction of new generation " << std::endl;
			Track_generation_number++; // update generation number
			record_reproduction = 1;
			for (int i = 0; i < gclList.size(); i++)
			{
				// allow preys to eat if they choose to
				gclList[i]->add_offspring(Sd_mum_offspring_vigilance, Mutation_rate);
			}
		}
		else
		{
			record_reproduction = 0;
		}

	} // closind for loop time steps

std::cout << "Simulation has terminated succesfully!" << std::endl;

res_text << std::endl;
res_text.close();
    return 0;
}// closing main
