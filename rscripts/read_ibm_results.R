# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Read in the results of the ibm
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
library(data.table)

par <- read.csv("src_cpp_code/parameters.csv", header = F, sep = ",")
names(par) <- c("Variable", "Value")
dt <- fread("src_cpp_code/results_simulation.txt", header = T, sep = ",")
unique(dt[,c("Simulation_time", "Generation_number", "New_generation")])